#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import googlemaps

def create_gmaps(api_key):
    return googlemaps.Client(key=api_key)

def geocode_reverse_geocode(df, gmaps, readcolumns, reverse=False):
    lista = []
    for index, row in df.iterrows():
        try:

            if reverse:
                list_columns = readcolumns.split(",")
                geocode_result = gmaps.reverse_geocode((row[list_columns[0]], row[list_columns[1]]))
                address_list = []
                for x in geocode_result[0]['address_components']:
                    address_list.append(x['long_name'])
                    address_list_string = ','.join(address_list)
                lista.append({'new_address': address_list_string})
            else:
                geocode_result = gmaps.geocode(row[readcolumns])
                lista.append({'lat': geocode_result[0]['geometry']['location']['lat'],
                              'lng': geocode_result[0]['geometry']['location']['lng']})
        except:
            lista.append('N/A')
    return pd.concat([df, pd.DataFrame(lista)], axis=1)