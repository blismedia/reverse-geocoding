# DCS-5769 Reverse GeoCoding

Use csv as input based on the example of `read_template.csv`.
It does not mean it has to be exactly like it but depending on the process:

- geocode would need an address column

- reverse_geocode would need two columns of lat,lng 


If something not working make sure you check the GOOGLE API in `config.py`.

When you want to geocode
```
python geocoding.py
--readpath /home/lefteris/git_projects/reverse-geocoding/read_template.csv
--readcolumn "Full Address"
--savepath /home/lefteris/git_projects/reverse-geocoding/write_template.csv
```
The result will be the same csv with two extra columns `lat`, `lng`

When you want to reverse geocode
```
python geocoding.py
--readpath /home/lefteris/git_projects/reverse-geocoding/read_template.csv
--readcolumn "lat,lng"
--savepath /home/lefteris/git_projects/reverse-geocoding/write_template.csv
--reverse
```
The result will be the same csv with one extra column `new_address`