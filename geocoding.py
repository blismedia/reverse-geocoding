#!/usr/bin/env python
# -*- coding: utf-8 -*-

from src.generalFunctions import *
from config import *
import click


@click.command()
@click.option('--readpath', required=True,help='Path of the file to read')
@click.option('--readcolumn', required=True, help='column of the address')
@click.option('--reverse/--no-reverse', default=False)
@click.option('--savepath', required=True,help='Path to save the file')
def run(readpath, readcolumn, reverse, savepath):
    '''
    :param readcolumn: if reverse then write the column names of latitude and
    longitude seperated by comma e.g. "lat,lon"
    '''
    df_csv = pd.read_csv(readpath)
    gmaps = create_gmaps(API_KEY)

    if reverse:
        coords = geocode_reverse_geocode(df_csv, gmaps, readcolumn, reverse=True)
    else:
        coords = geocode_reverse_geocode(df_csv, gmaps, readcolumn)

    coords.to_csv(savepath)


if __name__=='__main__':
    run()